require 'spec_helper'

describe "Static pages" do

  subject { page }
  
  # the short syntax
  shared_examples_for "all static pages" do
    it { should have_selector('h1', text: heading) }
    it { should have_title(full_title(page_title)) }
  end

  describe "FAQ page" do
  before { visit faq_path }
  
    let(:heading)    { 'Frequently Asked Questions' }
    let(:page_title) { 'FAQ' }

    it_should_behave_like "all static pages"
       
  end
  
  describe "About page" do
  before { visit about_path }
    
    let(:heading)    { 'About the Project' }
    let(:page_title) { 'About' }

    it_should_behave_like "all static pages"

  end
  
  describe "Contact page" do
  before { visit contact_path }
    
    let(:heading)    { 'Contact' }
    let(:page_title) { 'Contact' }

    it_should_behave_like "all static pages"
    
  end
  
  describe "Project Overview" do
  before { visit project_overview_path }
    
    let(:heading)    { 'Project Overview' }
    let(:page_title) { 'Project Overview' }

    it_should_behave_like "all static pages"
    
  end

  describe "User Signup" do
    before { visit user_signup_path }

    let(:heading)    { 'User Signup' }
    let(:page_title) { 'User Signup' }

    it_should_behave_like "all static pages"

  end

  describe "Analysis Pipeline" do
  before { visit analysis_pipeline_path }
    
    let(:heading)    { 'Analysis Pipeline' }
    let(:page_title) { 'Analysis Pipeline' }

    it_should_behave_like "all static pages"

  end
  
  describe "Gene Selection" do
  before { visit gene_selection_path }

    let(:heading)    { 'Gene Selection' }
    let(:page_title) { 'Gene Selection' }

    it_should_behave_like "all static pages"    
    
  end
  
  describe "Data Information" do
  before { visit data_information_path }

    let(:heading)    { 'Data Information' }
    let(:page_title) { 'Data Information' }

    it_should_behave_like "all static pages"     
    
  end
  
end
