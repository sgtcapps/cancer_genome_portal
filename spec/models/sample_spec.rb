require 'spec_helper'

describe Sample do
    before do
      # needs to be entered correctly or it will fail
      @sample = Sample.new(sample_code: "TCGA-AC-A3YJ")
    end

  subject { @sample }

  it { should respond_to(:sample_code) }

  it { should be_valid } 

  describe "when sample_code is not present" do
    before { @sample.sample_code = "" }
    it { should_not be_valid }
  end

  describe "when sample code is already taken" do
    before do

      sample_same = @sample.dup
      #sample_same = Sample.new(sample_code: "TCGA-BD-A3YK") #should cause fail
      sample_same.save
    end

    it { should_not be_valid } #comparing to the object that just got saved
  end

end
