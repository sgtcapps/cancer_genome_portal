require 'spec_helper'

describe DataSet do
    before do
      # needs to be entered correctly or it will fail
      @data_set = DataSet.new(analysis_date: "2013-01-31")
    end

  subject { @data_set }

  it { should respond_to(:analysis_date) }

  it { should be_valid } 

  describe "when analysis_date is not present" do
    before { @data_set.analysis_date = "" }
    it { should_not be_valid }
  end

  describe "when analysis_date format is valid" do
    it "should be valid" do
      dates = ['2121-12-12', '2013/02/11', '_1959\Jun2', 'Dec.10,2013']
      dates.each do |valid_date|
        @data_set.analysis_date = valid_date
        should be_valid
      end
    end
  end

  describe "when analysis_date format is invalid" do
    it "should be invalid" do
      dates = ['Todays date', '-1991/1/22']
      dates.each do |invalid_analysis_date|
        @data_set.analysis_date = invalid_analysis_date
        should_not be_valid
      end
    end
  end

  describe "when analysis_date is already taken" do
    before do
      data_set_same = @data_set.dup
      #data_set_same.analysis_date = 'April 1, 2021' #causes a fail because is unique
      data_set_same.save
    end

    it { should_not be_valid } #comparing to the object that just got "saved"
  end

end
