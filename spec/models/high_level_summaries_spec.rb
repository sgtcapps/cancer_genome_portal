require 'spec_helper'

describe HighLevelSummary do
# this model has undergone some changes...this validation no longer valid
=begin
    before do
      # needs to be entered correctly or it will fail
      @hi_lev_sum = HighLevelSummary.new(ClinicalParameter: "RPPAclust-Her2VsOthers")
    end

  subject { @hi_lev_sum }

  it { should respond_to(:ClinicalParameter) }

  it { should be_valid } 

  describe "when ClinicalParameter is not present" do
    before { @hi_lev_sum.ClinicalParameter = "" }
    it { should_not be_valid }
  end

  describe "when ClinicalParameter code is already taken" do
    before do

      hi_lev_sum_same = @hi_lev_sum.dup
      #hi_lev_sum_same = HighLevelSummary.new(ClinicalParameter: "RPPAclust-His2VsOthers")
      hi_lev_sum_same.save
    end

    it { should_not be_valid } #comparing to the object that just got saved
  end
=end
end
