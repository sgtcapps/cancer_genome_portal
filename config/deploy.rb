#####################deploy.rb######################

# deploy.rb

role :app, "toro.stanford.edu"

role :web, "toro.stanford.edu"

# use correct RVM

require 'rvm1/capistrano3'

set :rvm1_ruby_version, "2.4.2"

# capistrano bundler linked dirs

set :linked_dirs, fetch(:linked_dirs, []) << '.bundle'

#set :user, "jpalm"
set :user, "lucascj"

set :ssh_options, { :forward_agent => true }

#default_run_options[:pty] = true

set :deploy_to, "/opt/cancer_genome_portal"

set :use_sudo, false

#set :scm, "git"

set :repo_url, "bitbucket.org/sgtcapps/cancer_genome_portal.git"

set :branch, "master"

set :deploy_via, :remote_cache  # Just copy new/changed objects

#set :git_shallow_clone, 1      # Pull down entire clone, but just top commit

set :keep_releases, 10

# files we want symlinking to specific entries in shared

set :linked_files, %w{config/database.yml config/secrets.yml}


# dirs we want symlinking to shared

set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle}


#namespace :deploy do

#  after 'symlink:shared', 'compile_assets_locally'

#  after  :finishing,    :compile_assets

#  after  :finishing,    :cleanup

#end