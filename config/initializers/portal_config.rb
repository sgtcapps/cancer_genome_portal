# Set config variable with information specific to this portal (eg IPG, GCR)
module Portal
  class Config
    portals = YAML.load_file('config/portals.yml')
    this_portal = File.open('public/portal_info.txt', &:readline).chomp
    Info = portals[this_portal.downcase]
  end
end
