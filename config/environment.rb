# Load the Rails application.
require_relative 'application'

# Initialize the Rails application.
Rails.application.initialize!

ActionMailer::Base.default :from => "Sue Grimes <sgrimes@stanford.edu>"
