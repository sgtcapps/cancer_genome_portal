require_relative 'boot'

require 'rails/all'
require 'yaml'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module CancerPortal
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.1

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set mailer parameters based on info in mailer_settings.yml
    config.action_mailer.delivery_method     = config_for(:mailer_settings)[:method]
    config.action_mailer.default_url_options = config_for(:mailer_settings)[:url_options]
    if config_for(:mailer_settings)[:method] == :smtp
      config.action_mailer.smtp_settings = config_for(:mailer_settings)[:smtp_settings]
    end

  end
end
