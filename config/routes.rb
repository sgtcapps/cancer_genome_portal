Rails.application.routes.draw do
  
  devise_for :users, controllers: {
      sessions: 'users/sessions',
      registrations: 'users/registrations',
      passwords: 'users/passwords'
      }

  root to: 'welcome#index', via: [:get, :post]
  match 'param_entry', to: 'welcome#param_inputs', via: [:get, :post]

  # Static Pages
  match 'contact', to: 'static_pages#contact', via: 'get'
  match 'project_overview', to: 'static_pages#project_overview', via: 'get'
  match 'data_release', to: 'static_pages#data_release', via: 'get'
  match 'user_signup', to: 'static_pages#user_signup', via: 'get'

  #match 'ppt_sendfile' => 'static_pages_imh#ppt_sendfile', via: 'get'
  #match 'send_zip_file' => 'static_pages_imh#send_zip_file', via: [:get, :post]

  # Study Queries
  match 'study_query', to: 'study_queries#index', via: [:get, :post]
  match 'study_explore', to: 'explore_studies#index', via: [:get, :post]

  # Gene Queries
  match 'gene_summary', to: 'gene_summaries#index', via: [:get, :post]
  #match 'send_file' => 'gene_summaries#send_file', via: [:get, :post]

  # Neoantigen/HLA queries
  match 'neoantigen_query', to: 'neoantigen_queries#index', via: [:get, :post]

  # Sample query
  match 'sample_query', to: 'sample_queries#index', via: [:get, :post]
  get 'get_gene_expr_data', to: 'sample_queries#get_gene_expr_data'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end
  
  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
