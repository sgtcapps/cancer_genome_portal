class ExploreStudiesController < ApplicationController
  include ApplicationHelper
  include Statistics
  before_action :authenticate_user!, if: :login_required

  def index
    num_cancers = 1
    @cancer_study_id = params[:cancer_studies]
    @selected_study = CancerStudy.where({id: @cancer_study_id}).first
    @cancer = @selected_study.study_name
    @num_samples = @selected_study.total_samples
    @total_mut_samples = @selected_study.num_mut_samples
    @total_cn_samples = @selected_study.num_cn_samples
    @study_fmt = (['TCGA', 'GCR'].include?(@selected_study.study) ? @selected_study.study.downcase : 'ipg')

    # For gene summary tab
    gene_info = GeneSummary.includes(:gene).references(:gene).where({cancer_study_id: @cancer_study_id})
    if num_cancers == 1  #Leaving code here for multiple cancers in case we want to include option at some point
      @gene_mutations = gene_info.where("tot_mut_sample > 0 OR genes.cancer_assoc = 'Y'")
                          .order('tot_mut_sample DESC')
                          .pluck(:mut_rank, :gene_name, 'genes.cancer_assoc', 'genes.cytoband', 'genes.oncogene', 'genes.tumor_suppressor',
                                 :tot_mut_sample)
      @gene_cnvs = gene_info.where("tot_cnv_sample > 0 OR genes.cancer_assoc = 'Y'")
                         .order('tot_cnv_sample DESC')
                         .pluck(:cnv_rank, :gene_name, 'genes.cancer_assoc', 'genes.cytoband', 'genes.oncogene', 'genes.tumor_suppressor',
                                :tot_cnv_sample, :num_amp_sample, :num_del_sample)

      # Determine cutoff for 'top' genes and CNVs (>10% or >1 sample, or ># samples of 100th gene in rank order)
      mut_sample_cutoff = top_gene_threshold(@selected_study.num_mut_samples, @gene_mutations[99][6])
      cnv_sample_cutoff = top_gene_threshold(@selected_study.num_cn_samples, @gene_cnvs[99][6])
      @top_gene_thresholds = [mut_sample_cutoff, @selected_study.num_mut_samples,
                              cnv_sample_cutoff, @selected_study.num_cn_samples]
      @gene_mutations.select! {|g| (g[6] >= mut_sample_cutoff || g[2] == 'Y')}
      @gene_cnvs.select! {|g| (g[6] >= cnv_sample_cutoff || g[2] == 'Y')}

    else
      gene_mut_totals = gene_info.group(:gene_name)
                            .pluck(:mut_rank, :gene_name, 'genes.cancer_assoc', 'genes.cytoband', 'genes.oncogene', 'genes.tumor_suppressor',
                                   'SUM(tot_mut_sample)')
      gene_cnv_totals = gene_info.group(:gene_name)
                            .pluck(:cnv_rank, :gene_name, 'genes.cancer_assoc', 'genes.cytoband', 'genes.oncogene', 'genes.tumor_suppressor',
                                   'SUM(tot_cnv_sample)', 'SUM(num_amp_sample)', 'SUM(num_del_sample)')
      @gene_mutations = gene_mut_totals.sort_by {|k| k[6]}.reverse.first(25)
      @gene_cnvs = gene_cnv_totals.sort_by{|k| k[6]}.reverse.first(25)
    end

    # For clinical tab
    patient_obj = Patient.where({cancer_study_id: @cancer_study_id})
    patient_info = patient_obj.to_a
    @gender_counts = format_for_pie(patient_info, 'gender')
    @smoking_counts = format_for_pie(patient_info, 'smoking_history')
    @ages = patient_info.map {|pt| pt[:dx_age].to_i if pt[:dx_age] != 'NA'}.compact
    #IPG only, except site_counts for TCGA also:
    @site_counts = format_for_pie(patient_info, 'cancer_site')
    @stage_counts = format_for_pie(patient_info, 'summary_stage')
    #GCR only, except race_ethnicity for TCGA also
    @race_counts = format_for_pie(patient_info, 'race_ethnicity')
    @family_history_cts = patient_obj.group_by {|p| p.gc_history}.sort.map{|key, val| {name: key, y: val.size}}
    @site_dtl_counts = group_for_pie(patient_info, 'cancer_site_dtl')
    @diagnosis_counts = format_for_pie(patient_info, 'diagnosis','N')
    @diagnosis_dtl_counts = group_for_pie(patient_info, 'diagnosis_subtype')
    @differentiation_cts = format_for_pie(patient_info, 'differentiation')
    #TCGA only:
    @survival_mths = patient_info.map {|pt| (pt[:days_survival]/12.0).round(0).to_i if !pt[:days_survival].nil? }.compact
    @bmi = patient_info.map {|pt| pt[:dx_bmi].to_f.round(1) if pt[:dx_bmi] != 'NA'}.compact

    # Patient clinical table
    @patient_data = patient_info

    # Samples/sequencing table
    @cohort_samples = CohortSample.where({cancer_study_id: @cancer_study_id})
                      .order(:patient_nr, :sample_nr).to_a

    # For microbiome tab
    mbiome_info = SampleTaxonomy.includes(:sample).references(:sample)
                       .where({cancer_study_id: @cancer_study_id, taxonomy_level: 1})
                       .order(:patient_nr, :sample_type).to_a
    mbiome_sample_keys = mbiome_info.collect{|rec| [rec.patient_nr, rec.sample_type, rec.sample_id, rec.sample.patient_id]}.uniq
    bacteria_totals = SampleTaxonomy.sample_bacteria_rds(@cancer_study_id)

    @mbiome_perc = []; @mbiome_colors = [];
    phylum_names = SampleTaxonomy.all_phylum(@cancer_study_id)

    for phylum in phylum_names
      mbiome_data = []
      #phylum_color_idx = Hash[Phylum::COLOR_INDICES.map{|k,v| [k.sub(/ \((\d+)\)/,''), v]}]
      #cidx = phylum_color_idx[phylum[3..-1]]
      cidx = Phylum::COLOR_INDICES[phylum[3..-1]]
      @mbiome_colors.push(PlotInfo::PLOT_COLORS[:multi_20][cidx-1])
      phylum_data = mbiome_info.select{|mb| mb[:taxo_name] == phylum}
      for sample in mbiome_sample_keys
        sample_phylum = phylum_data.select{|pd| pd[:patient_nr] == sample[0] and pd[:sample_type] == sample[1]}
        mbiome_data.push(sample_phylum.empty? ? 0 : format_pct(sample_phylum[0][:read_cts], bacteria_totals[sample[2]],2))
      end
      @mbiome_perc.push({name: phylum[3..-1], data: mbiome_data})
    end

    @mbiome_samples = mbiome_sample_keys.map{|a| [a[0], a[1]].join('-')}
    @mbar_height = (@mbiome_samples.size > 0 ? @mbiome_samples.size * 30 + 200 : 500)

    # Microbiome/h.Pylori table (this should actually only be for GCR?)
    # Total bacterial reads in bacteria_totals; Proteobacteria reads per sample from mbiome_info
    # Total reads for Helicobacter need to come from sample_genera table
    proteobacteria_cts = mbiome_info.select{|mb| mb[:taxo_name] == 'p__Proteobacteria'}
    helicobacter_cts = SampleGenus.sample_genus_rds(@cancer_study_id, "Helicobacter")

    @hpylori_table = []
    for sample in mbiome_sample_keys
      all_rds = bacteria_totals[sample[2]]
      proteo_rds = proteobacteria_cts.select{|arr| arr[:sample_id] == sample[2]}[0].read_cts
      helico_rds = helicobacter_cts[[sample[2], 'g__Helicobacter']]

      s_hash = {patient_id: sample[3],
                sample_name: [sample[0], sample[1]].join('-'),
                sample_id: sample[2],
                all_rds: all_rds,
                proteo_rds: proteo_rds,
                p_pct: format_pct(proteo_rds, all_rds, 1),
                helico_rds: helico_rds,
                h_pct1: format_pct(helico_rds, proteo_rds, 1),
                h_pct2: format_pct(helico_rds, all_rds, 1)}
      @hpylori_table.push(s_hash)
    end

    # For immune cell tab
    immune_info = SampleImmuneCell.where({cancer_study_id: @cancer_study_id}).order(:patient_nr, :sample_type)
    @immune_samples = immune_info.map{|rec| [rec.patient_nr, rec.sample_type].join('-')}
    #@immune_tn = immune_info.pluck(:sample_type)

    @immune_perc = []
    flds = SampleImmuneCell::CIBERSORT_HDGS.keys.reverse

    for fld in flds
      @immune_perc.push({name: SampleImmuneCell::CIBERSORT_HDGS[fld],
                         data: immune_info.collect{|rec| (rec.send(fld).to_f)*100 }})
    end
    @bar_height = (@immune_samples.size > 0 ? @immune_samples.size * 30 + 200 : 500)

    @immune_hmap = []
    immune_info.each_with_index do |sample, i|
      sample_hmap = flds.collect{|fld| [flds.find_index(fld), i, sample.send(fld)]}
      @immune_hmap.push(sample_hmap)
    end
    @immune_hmap.flatten!(1)

    immune_vals = @immune_hmap.collect {|sample| sample[2]}.flatten
    @immune_hmap_max = immune_vals.max
    @immune_hmap_min = immune_vals.min  #Probably min is always zero?

    @immune_cells = flds
    #Heatmap container height = pixels per sample row, plus pixels for title and x-axis labels at top/bottom
    @hmap_height = (@immune_samples.size > 0 ? @immune_samples.size * 25 + 500 : 500)
    #@grouped_categories = grouped_categories(immune_info, 'sample_type', 'patient_nr')

    # For HLA types tab
    patient_hla_info = Patient.where({cancer_study_id: @cancer_study_id}).includes(:hla_types).references(:hla_types)
    @hla_counts = patient_hla_info.group(:hla_allele).count(:patient_id).map {|key, val| {name: key, y: val}}
    @hla_counts.reject! {|hla| hla[:name].nil?}

    @patient_hla = patient_hla_info.group_by{|patient| patient.patient_nr}
  end

  def top_gene_threshold(study_samples, nr_samples_100rank)
    ten_pct = (study_samples * 0.1).round
    return [[ten_pct, nr_samples_100rank].min(), 2].max()
  end

  def format_for_pie(patient_info, fld, sortkeys='Y')
    #patient_info.group(fld.to_sym).count(:id).map {|key, val| {name: key, y: val}}
    if sortkeys == 'Y'
      pt_sorted = patient_info.group_by {|p| p[fld.to_sym]}.sort.map{|key, val| {name: (key == 'U' ? 'NA' : key.to_s.upcase_first), y: val.size}}
    else
      pt_sorted = patient_info.group_by {|p| p[fld.to_sym]}.map{|key, val| {name: (key == 'U' ? 'NA' : key.to_s.upcase_first), y: val.size}}
    end
    pt_sorted.sort_by.with_index do |v,i|
      ["Other", "Unknown"].include?(v[:name]) ? (pt_sorted.length + i) : i
    end
  end

  def group_for_pie(patient_info, fld)
    patient_data = patient_info.reject{|patient| patient[fld.to_sym].nil? }
    if patient_data.nil? or patient_data.empty?
      patient_fld_split = []
    else
      patient_fld_split = patient_data.map {|patient| patient[fld.to_sym].split(',').map(&:strip)}.flatten
      patient_fld_split.sort.group_by { |x| x }.map { |k, v| {name: k.to_s.upcase_first, y: v.size} }
    end
  end

  def not_used_grouped_categories(immune_info, fld1, fld2)
    #return immune_info.select(fld1, fld2).group(fld1.to_sym).map {|key, val| {name: key, categories: val} }
    immune_sorted = immune_info.select(fld1, fld2).sort_by {|rec| [rec[fld1.to_sym], rec[fld2.to_sym]]}
    immune_grouped = immune_sorted.group_by {|rec| rec[fld1.to_sym]}
    return immune_grouped.map {|grp, dtl| {name: grp, categories: dtl.collect {|pt| pt[fld2]}}}
  end
end
