class StudyQueriesController < ApplicationController
  before_action :authenticate_user!, if: :login_required

  def index
      if params[:query_params] and params[:query_params].has_key?(:gene_name) and !params[:query_params][:gene_name].to_s.empty?
        redirect_to gene_summary_path(cancer_studies: params[:cancer_studies], gene_names: params[:query_params][:gene_name])
      elsif params[:query_params] and params[:query_params].has_key?(:neoantigen) and !params[:query_params][:neoantigen].to_s.empty?
        redirect_to neoantigen_query_path(cancer_studies: params[:cancer_studies], hla_id: params[:query_params][:neoantigen])
      elsif params[:query_params] and params[:query_params].has_key?(:patient_id) and !params[:query_params][:patient_id].to_s.empty?
        redirect_to sample_query_path(cancer_studies: params[:cancer_studies], patient_id: params[:query_params][:patient_id])
      elsif params[:cancer_studies]
        redirect_to study_explore_path(cancer_studies: params[:cancer_studies])
      else
        redirect_to root_url
      end
  end

end
