module Statistics
  extend ActiveSupport::Concern

  def min_Qn_max(array)  #From: https://rosettacode.org/wiki/Fivenum#Ruby
    if array.size == 0
      return [0,0,0,0,0]
    end

    sorted_arr = array.sort
    n = array.size
    n4 = (((n + 3).to_f / 2.to_f) / 2.to_f).floor
    d = Array.[](1, n4, ((n.to_f + 1) / 2).to_i, n + 1 - n4, n)
    sum_array = []
    (0..4).each do |e| # each loops have local scope, for loops don't
      index_floor = (d[e] - 1).floor
      index_ceil  = (d[e] - 1).ceil
      sum_array.push(0.5 * (sorted_arr[index_floor] + sorted_arr[index_ceil]))
    end

    # If interquartile range min/max needed, adjust sum_array min and max values using iqr_min, iqr_max
    # Also would need to collect and return outlier points (array values < iqr_min or > iqr_max)
    # array [min, q1, median, q3, max]
    iqr_fence = (sum_array[3]-sum_array[1]) * 1.5
    iqr_min = [sum_array[1] - iqr_fence, sum_array[0]].max
    iqr_max = [sum_array[3] + iqr_fence, sum_array[4]].min
    sum_array[0] = iqr_min
    sum_array[4] = iqr_max
    #outliers = sorted_arr.select do |fpkm|
    #  fpkm < iqr_min || fpkm > iqr_max
    #end

    sum_array.map{|arr| arr.to_f.round(3)}
  end

  def calc_rank(vector)
    sorted = vector.sort.uniq.reverse
    vector.map{|e| sorted.index(e) + 1}
  end
end
