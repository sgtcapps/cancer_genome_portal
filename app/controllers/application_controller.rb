class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :reset_session

  helper_method [
    :to_csv, :login_required, :sql_where
  ]
  
protected
  def login_required
    Portal::Config::Info['login_required']
  end

  def to_csv(file_name, array)
    require 'csv'    
    file = CSV.generate do |csv|
        #csv << header if not header.blank?
        csv << array.shift
        array.map {|row| csv << row}
    end
    file
  end

  def sql_where(model, conditions)
    sql_select = model.where(conditions).to_sql
    where_clause = 'WHERE ' + sql_select.split('WHERE')[1]
  end

end
