class NeoantigenQueriesController < ApplicationController
  include Statistics
  before_action :authenticate_user!, if: :login_required

  def index
    @cancer_study_ids = params[:cancer_studies]
    @selected_studies = CancerStudy.where({id: @cancer_study_ids})
    @cancers = @selected_studies.map{|cstudy| cstudy.study_name }
    @num_samples = @selected_studies.sum(:total_samples)

    @hla_id = params[:hla_id]
    @hla_type = HlaType.find(@hla_id)

    patient_hla = Patient.includes(:patient_hla_types).references(:patient_hla_types)
                  .where("hla_type_id = ? and patients.cancer_study_id IN (?)", @hla_id, @cancer_study_ids).all
    @patient_hla_ct = patient_hla.size

    @neoantigens = SampleHlaBinding.includes(:sample_neoantigen => {:sample_mutation => :cancer_study})
                       .references(:sample_neoantigen => {:sample_mutation => :cancer_study})
                       .where("hla_type_id = ? and sample_neoantigens.cancer_study_id IN (?)", @hla_id, @cancer_study_ids).all
  end
end
