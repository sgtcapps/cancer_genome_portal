class GeneSummariesController < ApplicationController
  include Statistics
  before_action :authenticate_user!, if: :login_required

  def index
    @sample_limit = 5000 #5000
    @genes, invalid_genes = parse_genes(params[:gene_names])
    unless invalid_genes.empty?
      flash[:error] = "The following genes are invalid: #{invalid_genes.join(',')}"
      redirect_to root_url(invalid_genes: invalid_genes.join(','))
      return
    end

    @cancer_study_ids = params[:cancer_studies]
    @selected_studies = CancerStudy.where({id: @cancer_study_ids})
    @num_cancers = @selected_studies.size
    @num_samples = @selected_studies.sum(:total_samples)
    @cancers = @selected_studies.map{|cstudy| cstudy.study_name }

    conditions = {gene_name: @genes, cancer_study_id: @cancer_study_ids}
    @gene_summary = GeneSummary.includes(:cancer_study).where(conditions).order(:gene_name,:cancer_study_id)

    @categories = @gene_summary.collect{|gsum| ['[', gsum.gene_name, '] ', '(',gsum.cancer_study.cancer_abbr,'|',gsum.cancer_study.study,')'].join}
    @num_categories = @categories.size

    # For genomic summary stacked bar chart
    @counts = []; @counts_perc = [];
    chart_hdgs_mut = {'num_missense' => 'Missense Mutations', 'num_frameshift' => 'Frameshift Mutations', 'summary_other_mut' => 'Other Mutations',
                      'num_inframe_indel' => 'Inframe Indels', 'num_nonsense' => 'Nonsense', 'num_splice' => 'Splice',
                      'num_intron' => 'Intronic', 'num_utr_flank' => 'UTR/Flank', 'num_silent' => 'Silent',
                      'num_other_mut' => 'Other Misc'}
    chart_hdgs_cn  = {'num_amp_sample' => 'CN Amplifications', 'num_del_sample' => 'CN Deletions'}
    chart_hdgs     = chart_hdgs_mut.merge(chart_hdgs_cn)
    m_last_idx = chart_hdgs_mut.size - 1

    for fld in chart_hdgs.keys
      @counts.push({name: chart_hdgs[fld], data: @gene_summary.collect{|gsum| gsum.send(fld).to_i }})
      cnt_denom = (chart_hdgs_mut.keys.include?(fld) ? 'num_mut_samples' : 'num_cn_samples')
      @counts_perc.push({name: chart_hdgs[fld], data: @gene_summary.collect{|gsum| (100 * gsum.send(fld).to_i / gsum.cancer_study.send(cnt_denom).to_f).round(2) }})
    end

    # Summary stacked bar chart
    @summ_counts = @counts[0..2] + @counts[m_last_idx+1..-1]
    @summ_counts_perc = @counts_perc[0..2] + @counts_perc[m_last_idx+1..-1]

    # For mutation stacked bar chart (remove 'Mutations' suffix for first two entries)
    @mut_counts = @counts[0..1] + @counts[3..m_last_idx]
    @mut_counts[0][:name] = 'Missense'
    @mut_counts[1][:name] = 'FrameShift'

    @mut_counts_perc = @counts_perc[0..1] + @counts_perc[3..m_last_idx]
    @mut_counts_perc[0][:name] = 'Missense'
    @mut_counts_perc[1][:name] = 'FrameShift'

    # For copy number stacked bar chart
    @cn_counts = @counts[m_last_idx+1..-1]
    @cn_counts_perc = @counts_perc[m_last_idx+1..-1]

    # For genomic summary tabs
    @studies_with_details = @selected_studies.has_details
    if @num_samples <= @sample_limit and @studies_with_details.size > 0
      @filtered_study_ids = @studies_with_details.collect(&:id)
      @cstudies = CancerStudy.study_names(@filtered_study_ids)
      @gene_mut_data = SampleMutation.mut_tab_query(sql_where(SampleMutation, conditions))
      @gene_cn_data = SampleCnv.cnv_tab_query(sql_where(SampleCnv, conditions))
      @gene_exp_data = SampleRnaExpr.joins(:cancer_study).preload(:cancer_study).where(conditions).order(:cancer_study_id, :gene_name)
      @gene_neoantigens = SampleMutation.neoantigen_tab_query(sql_where(SampleMutation, conditions))

      # for RNA expression box plot
      @boxplot_detail = gene_study_init(@genes, @filtered_study_ids)
      @gene_exp_data.each do |gene_exp|
        @boxplot_detail[gene_exp.gene_name][gene_exp.cancer_study_id].push(gene_exp.htseq_fpkm)
      end

      # Need array of hashes in following format
      #[{name: 'WRN', data: [[760,801,848,895,965], [733,853,939,980,1080]]}, ..]
      @boxplot_data = []
      @genes.each_with_index do |gene, i|
        @boxplot_data[i] = {name: gene}
        @boxplot_data[i][:data] = @filtered_study_ids.collect{|study| min_Qn_max(@boxplot_detail[gene][study.to_i])}
      end

    end # /if num_samples
    # for debug
    #abort(@gene_neoantigens.inspect)
  end

  def parse_genes(gene_string)
    genes = gene_string.upcase.gsub(/\s+/, "").split(',')
    valid_genes = Gene.all_genes; invalid_genes = [];
    for gene in genes
      invalid_genes.push(gene) unless valid_genes.include?(gene)
    end
    return genes, invalid_genes
  end

  def gene_study_init(genes, studies)
    data_hash = {}
    genes.each do |gene|
      data_hash[gene.to_str] = {}
      studies.each do |study|
        data_hash[gene.to_str][study] = []
      end
    end
    data_hash
  end

end
