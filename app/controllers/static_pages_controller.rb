class StaticPagesController < ApplicationController

  def contact
    render "static_pages/contact_#{Portal::Config::Info['project']}"
  end

  def project_overview
    render "static_pages/project_overview_#{Portal::Config::Info['project']}"
  end

  def data_release
    @study_samples = Cohort.includes(:cancer_study).where(cancer_study_id: 1).all
    @tcga_samples = Cohort.includes(:cancer_study).where(cancer_study_id: [2,3,4]).all
    render "static_pages/data_release_#{Portal::Config::Info['project']}"
  end

  def user_signup
    render "static_pages/user_signup_#{Portal::Config::Info['project']}"
  end

end
