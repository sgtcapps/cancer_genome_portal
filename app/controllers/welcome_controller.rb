class WelcomeController < ApplicationController
  def index
    if !login_required or user_signed_in?
      #@grouped_cancer_studies = CancerType.cancer_study_dropdown
      @grouped_cancer_studies = CancerStudy.study_dropdown
      render :index
    else
      redirect_to '/users/sign_in'
    end
  end

  def param_inputs
    @query_type = params[:element_id]
    if @query_type == 'target_patient'
      @patient_nrs = Patient.populate_dropdown_grouped(params[:studies])
    elsif @query_type == 'target_neoantigen'
      @hla_types = SampleHlaBinding.hla_dropdown
    end
    respond_to do |format|
      format.js
    end
  end

end
