class SampleQueriesController < ApplicationController
  include Statistics
  require "csv"
  before_action :authenticate_user!, if: :login_required

  def index
    @pt_samples = Patient.joins(:cancer_study, :samples).find(params[:patient_id])
    if @pt_samples.nil?
      flash[:error] = "Patient number id: #{params[:patient_id]} not found"
      redirect_to root_url
      return
    end

    @cancer_study_id = params[:cancer_studies]
    @selected_study = CancerStudy.where({id: @cancer_study_id}).first
    @cancer = @selected_study.study_name
    @study_fmt = (['TCGA', 'GCR'].include?(@selected_study.study) ? @selected_study.study.downcase : 'ipg')

    @cancer_study = @pt_samples.cancer_study.study_name
    @patient_hla = Patient.includes(:hla_types).find(params[:patient_id])
    sample_ids = @pt_samples.samples.map{|sample| sample.id }
    @mutations = SampleMutation.joins(:gene).eager_load(:gene).where({sample_id: sample_ids})
    @cnvs = SampleCnv.joins(:gene).eager_load(:gene).where({sample_id: sample_ids})
    @neoantigens = SampleMutation.joins(:cancer_study, :gene, :sample_neoantigens => :sample_hla_bindings)
                       .eager_load(:cancer_study, :gene, :sample_neoantigens => :sample_hla_bindings)
                       .where({sample_id: sample_ids})

    #########Counts for mutation summary bar chart########
    #mut_sql_counts = SampleMutation.where({sample_id: [1508,1510]}).group(:patient_nr, :var_classification).count #For testing
    mut_sql_counts = @mutations.group(:patient_nr, :var_classification).count
    @mut_samples = mut_sql_counts.map{|k,v| k[0]}.uniq
    @mut_counts = []

    if @mut_samples.size > 0
      if @mut_samples.size == 1
        mut_variants = mut_sql_counts.map{|k,v| [k[1], v]}
        mut_hash = summarize_mut_cts(mut_variants, initialize_mut_cts(1), 0)
      else #@mut_samples.size > 1
        mut_hash = initialize_mut_cts(@mut_samples.size)
        @mut_samples.each_with_index do |sample, i|
          sample_mut_types  = mut_sql_counts.select{|k,v| k[0] == sample}.map{|k,v| [k[1],v]}
          mut_hash = summarize_mut_cts(sample_mut_types, mut_hash, i)
        end
      end
      @mut_counts = mut_hash.map{|k,v| {:name => k, :data => v}}
    end

    #########Counts for copy number summary bar chart########
    #cn_sql_counts = SampleCnv.where({sample_id: [1508,1510]}).group(:patient_nr, :copy_num).count #For testing
    #cn_sql_counts: {['P06051', -1]=>931, ['P06051', 1]=>3491, ['P06053',-1]=>532, ['P06053',1]=>917}
    cn_sql_counts = @cnvs.group(:patient_nr, :copy_num).count
    @cn_samples = cn_sql_counts.map{|k,v| k[0]}.uniq
    @cn_counts = []
    @no_cnv_text = 'No data available in table'

    if @cn_samples.size > 0
      if @cn_samples.size == 1
        cn_hash = cn_sql_counts.map{|k,v| [k[1], [v]]}
      else #@cn_samples.size > 1, sample has amplifications and deletions
        cn_hash = {1 => [0]*@cn_samples.size, -1 => [0]*@cn_samples.size}
        @cn_samples.each_with_index do |sample, i|
          sample_cn_types = cn_sql_counts.select{|k,v| k[0] == sample}.map{|k,v| [k[1], v]}
          cn_hash = summarize_cn_cts(sample_cn_types, cn_hash, i)
        end
      end
      @cn_counts = cn_hash.map{|k,v| {name: SampleCnv::CN_TRANSLATE[k], data: v}}.reverse
    else
      #sample has no CNV records, check if CNV processing performed for this patient
      #NOTE: currently only the GCR/STCA patients are in cohort_samples table
      sample_processing = CohortSample.where({cancer_study_id: @cancer_study_id, patient_nr: @pt_samples.patient_nr}).to_a
      if sample_processing.size > 0 and sample_processing[0].has_wgs?
        @no_cnv_text = 'No CNV regions for this sample'
      end
    end

    #########Counts for microbiome sankey plot and table########
    @mbiome_sankey = SampleTaxonomy.sankey_data(sample_ids)
    @genus_cts = SampleGenus.genus_cts(sample_ids)

    @sample_ids = sample_ids
    #@json = SampleRnaExpr.joins(:gene).where({sample_id: 1459}).limit(10).as_json(include: :gene)
    #render :debug
  end

  def get_gene_expr_data
    sample_id = params['sample_id'][1..-2]  #Strip brackets
    sample_ids = sample_id.split(',').map{|s| s.to_i }
    @rna_expr = SampleRnaExpr.joins(:gene).eager_load(:gene)
                    .where("sample_rna_expr.sample_id IN (?)", sample_ids)
                    .as_json(include: :gene)
    render json: {data: @rna_expr}
  end

  def initialize_mut_cts(nr_samples)
    mut_hash = {}
    chart_groups = ['Missense', 'Frameshift', 'Inframe Indels', 'Nonsense', 'Splice', 'Intronic',
                    'UTR/Flank', 'Silent', 'Other Misc']
    chart_groups.each do |hdg|
      mut_hash[hdg] = [0]*nr_samples
    end
    return mut_hash
  end

  def summarize_mut_cts(var_counts, mut_hash, idx)
    var_counts.each do |var|
      if var[0] == 'Missense_Mutation'
        mut_hash['Missense'][idx] = var[1]
      elsif var[0][0..10] == 'Frame_Shift'
        mut_hash['Frameshift'][idx] += var[1]
      elsif var[0][0..7] == 'In_Frame'
        mut_hash['Inframe Indels'][idx] += var[1]
      elsif var[0] == 'Nonsense_Mutation'
        mut_hash['Nonsense'][idx] = var[1]
      elsif var[0][0..5] == 'Splice'
        mut_hash['Splice'][idx] += var[1]
      elsif var[0] == 'Intron'
        mut_hash['Intronic'][idx] = var[1]
      elsif ['UTR', 'Flank'].include?(var[0][2..-1])
        mut_hash['UTR/Flank'][idx] += var[1]
      elsif var[0] == 'Silent'
        mut_hash['Silent'][idx] = var[1]
      else
        mut_hash['Other Misc'][idx] += var[1]
      end
    end
    return mut_hash
  end

  def summarize_cn_cts(cn_counts, cn_hash, idx)
    cn_counts.each do |var|
      cn_hash[var[0]][idx] = var[1]
    end
    return cn_hash
  end

  def debug
    #@mutations = []
    #@cnvs = []
    #@rna_expr = []
    #@neoantigens = []
    #@sql_statement = SampleCnv.joins(:gene).preload(:gene).where({sample_id: sample_ids}).to_sql
    #@sql_results   = SampleCnv.joins(:gene).preload(:gene).where({sample_id: sample_ids}).all
  end
end
