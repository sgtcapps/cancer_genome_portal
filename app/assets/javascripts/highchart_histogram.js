var hist_color = ["#2986e2"];

function hc_histogram(chart_id) {
    logger("hc_histogram(" + chart_id + ")");

    var chart_series = $('#'+chart_id).data('chart-series');
    var title_text   = $('#'+chart_id).data('title-text');
    var x_title   = $('#'+chart_id).data('x-axis');
    var bin_width = $('#'+chart_id).data('bin-size');

    histogram_options = {
        chart: {
            type: 'histogram',
            renderTo: chart_id,
            borderWidth: 4,
            borderColor: 'lightgray',
            spacingTop: 23,
        },
        colors: hist_color,
        title: {
            text: title_text,
            style: {
                fontSize: 22
            }
        },
        legend: {
            enabled: false
        },
        tooltip: {
            pointFormat: '<b>{point.x} - {point.x2}: {point.y} </b>'
        },
        xAxis: {
            title: {
                text: x_title,
                style: {
                    fontSize: 20
                }
            },
            labels: {
                style: {
                    fontSize: 15
                }
            }
        },
        yAxis: {
            title: {
                text: 'Count',
                style: {
                    fontSize: 20
                }
            },
            labels: {
                style: {
                    fontSize: 15
                }
            }
        },
        credits: {
            enabled: false
        },
        series: [{
            type: 'histogram',
            baseSeries: 's1',
            binWidth: bin_width
        },{
            data: chart_series,
            visible: false,
            id: 's1'
        }
        ]
    }
    return (histogram_options)
}
