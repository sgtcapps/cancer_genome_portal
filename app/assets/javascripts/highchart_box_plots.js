function hc_box_plot(chart_id) {
    logger("hc_box_plot(" + chart_id + ")");

    var x_categories = $('#'+chart_id).data('x-categories');
    var chart_series = $('#'+chart_id).data('chart-series');
    var title_text   = $('#'+chart_id).data('title-text');
    var y_title      = $('#'+chart_id).data('y-title');
    var chart_colors = $('#'+chart_id).data('chart-colors');
    var chart_width = $('#'+chart_id).data('chart-width');

    box_plot_options = {
        chart: {
            type: 'boxplot',
            width: chart_width,
            renderTo: chart_id
        },
        colors: chart_colors,
        plotOptions: {
            boxplot: {
                lineWidth: 3,
                medianWidth: 3,
                stemWidth: 3,
                whiskerWidth: 3
            },
            series: {
                groupPadding: 0.1
            }
        },
        title: { text: title_text },
        legend: {
            align: 'right',
            layout: 'vertical',
            verticalAlign: 'top',
            x: 0, y: 100,
            itemMarginTop: 5,
            itemMarginBottom: 5,
            itemStyle: { fontSize: '16px' }
            },
        xAxis: {categories: x_categories,
            labels: {
                style: { fontSize: '18px' }
                }
            },
        yAxis: {
            title: {
                text: y_title,
                style: { fontSize: '20px' }
                },
            min: 0,
            max: null,
            labels: {
                style: {
                    fontSize: '15px'
                }
            }
        },
        credits: { enabled: false },
        series: chart_series
    };
    return(box_plot_options)
}

function hc_array_box_plot(chart_id, data_id) {
    logger("hc_array_box_plot(" + chart_id + ")");

    var x_categories = $('#' + data_id).data('x-categories');
    var y_title = $('#' + data_id).data('y-title');
    var chart_colors = $('#' + data_id).data('chart-colors');
    var title_text = $('#' + chart_id).data('title-text');
    var chart_series = $('#' + chart_id).data('chart-series');

    box_plot_options = {
        chart: {
            type: 'boxplot',
            renderTo: chart_id
        },
        colors: chart_colors,
        plotOptions: {
            boxplot: {
                lineWidth: 3,
                medianWidth: 3,
                stemWidth: 3,
                whiskerWidth: 3
            }
        },
        title: {text: title_text},
        legend: false,

        xAxis: {
            categories: x_categories,
            labels: {
                style: {
                    fontSize: '13px'
                }
            }
        },
        yAxis: {
            title: {
                text: y_title,
                style: {
                    fontSize: '16px'
                }
            },
            min: 0,
            max: null,
            labels: {
                style: {
                    fontSize: '15px'
                }
            }
        },
        series: [chart_series],
        credits: {enabled: false}
    }
    return(box_plot_options)
}
