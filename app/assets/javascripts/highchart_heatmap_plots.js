function hc_heatmap(chart_id) {
    logger("hc_heatmap(" + chart_id + ")");

    var title_text   = $('#'+chart_id).data('title-text');
    var x_categories = $('#'+chart_id).data('x-categories');
    var chart_series = $('#'+chart_id).data('chart-series');
    var y_categories = $('#'+chart_id).data('y-categories');
    var hmap_min = $('#'+chart_id).data('hmap-min');
    var hmap_max = $('#'+chart_id).data('hmap-max');

    heatmap_options = {
        chart: {
            type: 'heatmap',
            marginRight: 30,
            renderTo: chart_id
        },
        legend: {
            layout: 'horizontal',
            borderWidth: 0,
            backgroundColor: 'rgba(255,255,255,0.85)',
            floating: true,
            verticalAlign: 'top',
            align: 'left',
            width: 400,
            symbolWidth: 390,
            symbolHeight: 20
        },
        colorAxis: {
            labels: {
                style: {
                    fontSize: 16
                }
            },
            min: hmap_min,
            max: hmap_max,
            stops: [
                [0, '#374292'], //blue
                [0.05, '#ffffff'], //white
                [0.3, '#932a15'] //red
            ]
        },
        yAxis: {
            categories: x_categories,
            labels: {
                style: {
                    fontSize: 13,
                    textOverflow: 'none'
                }
            },
            title: null,
            reversed: true
        },
        xAxis: [{
            categories: y_categories,
            labels: {
                rotation: 45,
                style: {
                    fontSize: 16,
                    textOverflow: 'none'
                }
            },
            title: null,
            reversed: true,
            max: 21
        },{
            linkedTo: 0,
            title: null,
            opposite: true,
            reversed: true,
            categories: y_categories,
            labels: {
                rotation: 315,
                style: {
                    fontSize: 16,
                    textOverflow: 'none'
                }
            }
        }],
        title: {
            text: title_text,
            style: {
                fontSize: 24
            }
        },
        tooltip: {
            formatter: function () {
                return '<b>' + getPointCategoryName(this.point, 'y') + '</b><br><b>' +
                    getPointCategoryName(this.point, 'x') + '</b><br>' +  this.point.value;
            }
        },
        series: [{
            borderWidth: 0.2,
            borderColor: 'black',
            data: chart_series,
            dataLabels: {
                enabled: false,
                color: '#000000',
                shadow: false,
                style: {
                    textOutline: null,
                    color: 'black'
                }
            }
        }],
        credits: {
            enabled: false
        }
    }
  return(heatmap_options)
}
