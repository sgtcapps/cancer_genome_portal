$.extend( true, $.fn.dataTable.defaults, {
    dom: "<'row'<'col-sm-6'l><'col-sm-6'f>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-6'i><'col-sm-2'B><'col-sm-4'p>>",
    buttons: [
        {
            extend: 'csv',
            text: 'Download'
        }
    ],
    text: 'Download',
    iDisplayLength: 50,
    aLengthMenu: [[25, 50, 75, 100, -1], [25, 50, 75, 100, "All"]]
} );
