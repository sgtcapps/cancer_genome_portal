function sample_queries_index_init() {
    logger("sample_queries_index_init()");

    //$('#selector_mutation button').on('click',(toggleDiv('mutation_perc', 'mutation_counts')))

    //Highcharts.setOptions({
    //    colors: bar8_colors
    //});
    //point_width = $('#bar_chart_options').data('point-width');

    // Mutation bar chart (gene counts)
    var mut_bar_id = document.getElementById('sample_mut_chart_container');
    if (mut_bar_id) {
        stacked_bar_options = hc_stacked_bar('sample_mut_chart_container', 30, 'top');
        new Highcharts.Chart(stacked_bar_options);
    };

    // Copy number bar chart (gene counts)
    var cnv_bar_id = document.getElementById('sample_cn_chart_container');
    if (cnv_bar_id) {
        stacked_bar_options = hc_stacked_bar('sample_cn_chart_container', 30, 'top');
        new Highcharts.Chart(stacked_bar_options);
    };
}
