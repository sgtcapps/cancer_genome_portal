function hc_pie_chart(chart_id) {
    logger("hc_pie_chart(" + chart_id + ")");

    var chart_series = $('#'+chart_id).data('chart-series');
    var title_text   = $('#'+chart_id).data('title-text');
    var chart_colors = $('#'+chart_id).data('chart-colors');

    pie_chart_options = {
        chart: {
            type: 'pie',
            plotBackgroundColor: null,
            borderWidth: 4,
            borderColor: 'lightgray',
            spacingTop: 23,
            plotShadow: false,
            renderTo: chart_id
        },
        colors: chart_colors,
        title: {
            text: title_text,
            style: {
                fontSize: 22
            }
        },
        tooltip: {
            pointFormat: '<b>{point.y}</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f}%',
                    style: {
                        fontSize: "14px",
                        width: "80px"
                    },
                    showInLegend: true
                },
                states: {
                    hover: {
                        enabled: true
                    }
                }

            }
        },
        credits: {
            enabled: false
        },
        series: [{
            colorByPoint: true,
            data: chart_series
        }]
    }
    return (pie_chart_options)
}

function hc_pie_wlegend(chart_id) {
    logger("hc_pie_chart(" + chart_id + ")");

    var chart_series = $('#'+chart_id).data('chart-series');
    var title_text   = $('#'+chart_id).data('title-text');
    var chart_colors = $('#'+chart_id).data('chart-colors');

    pie_chart_options = {
        chart: {
            type: 'pie',
            plotBackgroundColor: null,
            borderWidth: 4,
            borderColor: 'lightgray',
            spacingTop: 23,
            plotShadow: false,
            renderTo: chart_id
        },
        colors: chart_colors,
        title: {
            text: title_text,
            style: {
                fontSize: 22
            }
        },
        legend: {
            itemDistance: 10,
            alignColumns: true
        },
        tooltip: {
            pointFormat: '<b>{point.y} ({point.percentage:.1f}%)</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true,
                states: {
                    hover: {
                        enabled: true
                    }
                }

            }
        },
        credits: {
            enabled: false
        },
        series: [{
            colorByPoint: true,
            data: chart_series
        }]
    }
    return (pie_chart_options)
}
