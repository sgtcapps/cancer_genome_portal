function explore_studies_index_init() {
    logger("explore_studies_index_init()");
    // Pie charts
    pie_chart_options = hc_pie_chart('hla_a_container');
    pie_chart_options['plotOptions']['pie']['dataLabels']['format'] = '<b>{point.name}</b>';
    new Highcharts.Chart(pie_chart_options);

    pie_chart_options = hc_pie_chart('hla_b_container');
    pie_chart_options['plotOptions']['pie']['dataLabels']['format'] = '<b>{point.name}</b>';
    new Highcharts.Chart(pie_chart_options);

    pie_chart_options = hc_pie_chart('hla_c_container');
    pie_chart_options['plotOptions']['pie']['dataLabels']['format'] = '<b>{point.name}</b>';
    new Highcharts.Chart(pie_chart_options);

    heatmap_options = hc_heatmap('immune_hmap_container')
    new Highcharts.Chart(heatmap_options)
    immune_bar_options = hc_stacked_bar_legendRight('immune_chart_container', 5);
    new Highcharts.Chart(immune_bar_options);

    //mbiome_bar_options = hc_stacked_column('mbiome_chart_container');
    mbiome_bar_options = hc_stacked_bar_legendRight('mbiome_chart_container', 10)
    new Highcharts.Chart(mbiome_bar_options);
}

function explore_studies_tcga() {
    pie_chart_options = hc_pie_chart('gender_pie_container');
    new Highcharts.Chart(pie_chart_options);

    pie_chart_options = hc_pie_wlegend('race_ethno_container');
    new Highcharts.Chart(pie_chart_options);

    pie_chart_options = hc_pie_chart('stage_pie_container');
    new Highcharts.Chart(pie_chart_options);

    if (document.getElementById('site_pie_container')) {
        pie_chart_options = hc_pie_wlegend('site_pie_container');
        new Highcharts.Chart(pie_chart_options);
    }

    if (document.getElementById('age_histogram_container')) {
        histogram_options = hc_histogram('age_histogram_container');
        new Highcharts.Chart(histogram_options);
    }

    histogram_options = hc_histogram('survival_histogram_container');
    new Highcharts.Chart(histogram_options);
}

function explore_studies_imh() {
    pie_chart_options = hc_pie_chart('gender_pie_container');
    new Highcharts.Chart(pie_chart_options);

    pie_chart_options = hc_pie_chart('smoking_pie_container');
    new Highcharts.Chart(pie_chart_options);

    pie_chart_options = hc_pie_chart('stage_pie_container');
    new Highcharts.Chart(pie_chart_options);

    pie_chart_options = hc_pie_wlegend('site_pie_container');
    new Highcharts.Chart(pie_chart_options);

    histogram_options = hc_histogram('age_histogram_container');
    new Highcharts.Chart(histogram_options);

    histogram_options = hc_histogram('survival_histogram_container');
    new Highcharts.Chart(histogram_options);
}

function explore_studies_gcr() {
    pie_chart_options = hc_pie_chart('gender_pie_container');
    new Highcharts.Chart(pie_chart_options);

    pie_chart_options = hc_pie_wlegend('race_ethno_container');
    new Highcharts.Chart(pie_chart_options);

    pie_chart_options = hc_pie_wlegend('site_dtl_container');
    new Highcharts.Chart(pie_chart_options);

    histogram_options = hc_histogram('age_histogram_container');
    new Highcharts.Chart(histogram_options);

    //histogram_options = hc_histogram('bmi_histogram_container');
    //new Highcharts.Chart(histogram_options);

    pie_chart_options = hc_pie_chart('fhistory_pie_container');
    new Highcharts.Chart(pie_chart_options);

    pie_chart_options = hc_pie_chart('smoking_pie_container');
    new Highcharts.Chart(pie_chart_options);

    pie_chart_options = hc_pie_wlegend('diagnosis_container');
    new Highcharts.Chart(pie_chart_options);

    pie_chart_options = hc_pie_wlegend('diagnosis_dtl_container');
    new Highcharts.Chart(pie_chart_options);

    pie_chart_options = hc_pie_wlegend('differentiation_container');
    new Highcharts.Chart(pie_chart_options);
}
