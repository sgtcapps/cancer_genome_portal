//Stacked bar with bottom legend
function hc_stacked_bar(chart_id, point_width, title_location) {
    logger("hc_stacked_bar(" + chart_id + ")");

    var x_categories = $('#'+chart_id).data('x-categories');
    var chart_series = $('#'+chart_id).data('chart-series');
    var title_text   = $('#'+chart_id).data('title-text');
    var chart_colors = $('#'+chart_id).data('chart-colors');
    var top_title    = (title_location === 'top') ? title_text : '';
    var bottom_title = (title_location === 'x-axis') ?  title_text : '';

    stacked_bar_options = {
        chart: {
            type: 'bar',
            zoomType: 'y',
            renderTo: chart_id
        },
        colors: chart_colors,
        title: {
            text: top_title,
            style: {
                fontSize: 24
            }
        },
        xAxis: {
            categories: x_categories,
            labels: {
                style: {
                    fontSize: '15px'
                }
            },
            title: {
                text: ''
            }
        },
        yAxis: {
            allowDecimals: false,
            reversedStacks: false,
            min: 0,
            title: {
                text: bottom_title,
                style: {
                    fontSize: '20px'
                }
            },
            labels: {
                style: {
                    fontSize: '15px'
                }
            }
        },
        legend: {
            reversed: false,
            itemStyle: {
                fontSize: '15px',
                itemMarginTop: 50,
                itemMarginBottom: 5
            }
        },
        plotOptions: {
            series: {
                stacking: 'normal',
                pointWidth: parseInt(point_width)
            }
        },
        credits: {
            enabled: false
        },
        series: chart_series
    }

    return(stacked_bar_options)
}

// Stacked bar with vertical legend on the right
function hc_stacked_bar_legendRight(chart_id, point_width) {
    logger("hc_stacked_bar_legendRight(" + chart_id + ")");

    var x_categories = $('#'+chart_id).data('x-categories');
    var chart_series = $('#'+chart_id).data('chart-series');
    var title_text   = $('#'+chart_id).data('title-text');
    var chart_colors = $('#'+chart_id).data('chart-colors');

    stacked_bar_options = {
        chart: {
            type: 'bar',
            renderTo: chart_id
        },
        title: {
            text: title_text,
            style: {
                fontSize: 24
            }
        },
        colors: chart_colors,
        legend: {
            layout: 'vertical',
            x: 0,
            y: 100,
            reversed: true,
            align: 'right',
            verticalAlign: 'top',
            itemMarginTop: 5,
            itemMarginBottom: 5,
            borderWidth: 1,
            symbolWidth: 12,
            symbolRadius: 0,
            squareSymbol: false
        },
        xAxis: {
            categories: x_categories,
            labels: {
                style: {
                    fontSize: 13
                }
            }
        },
        yAxis: [{
            title: {
                text: 'Percent',
                style: {
                    fontSize: 18
                }
            },
            labels: {
                style: {
                    fontSize: 16
                }
            },
            min: 0,
            max: 100
        },{
                linkedTo: 0,
            offset: -10,
                title: {
                    text: 'Percent',
                    style: {
                        fontSize: 18
                    }
                },
                opposite: true,
                reversed: true,
                labels: {
                    style: {
                        fontSize: 16,
                        textOverflow: 'none'
                    }
                }
            }],
        plotOptions: {
            series: {
                stacking: 'normal',
                pointPadding: 0,
                tooltip: {
                    useHTML: true,
                    headerFormat: '<b>{point.key}</b><br>',
                    pointFormat: '{series.name}: {point.y:.3f}'
                }
            }
        },
        series: chart_series,
        credits: {
            enabled: false
        }
    }
    return(stacked_bar_options)
}
