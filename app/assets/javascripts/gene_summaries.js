function gene_summaries_index_init() {
    logger("gene_summaries_index_init()");

    //$('#selector_mutation button').on('click',(toggleDiv('mutation_perc', 'mutation_counts')))

    //Highcharts.setOptions({
    //    colors: bar8_colors
    //});
    point_width = $('#bar_chart_options').data('point-width');
    title_position = 'x-axis'

    // Bar charts (counts)
    stacked_bar_options = hc_stacked_bar('gene_summary_chart_container', point_width, title_position);
    new Highcharts.Chart(stacked_bar_options);

    // Mutation counts
    stacked_bar_options = hc_stacked_bar('gene_mutation_chart_container', point_width, title_position);
    new Highcharts.Chart(stacked_bar_options);

    // Copy number counts
    stacked_bar_options = hc_stacked_bar('gene_cn_chart_container', point_width, title_position);
    new Highcharts.Chart(stacked_bar_options);

    // Expression box plots
    box_plot_options = hc_box_plot('fpkm_summary_boxplot_container');
    new Highcharts.Chart(box_plot_options);

    // Bar charts (percentages)
    Highcharts.setOptions({yAxis: {ceiling: 100}})
    stacked_bar_options = hc_stacked_bar('gene_summary_perc_chart_container', point_width, title_position);
    new Highcharts.Chart(stacked_bar_options);

    // Mutation percentages
    stacked_bar_options = hc_stacked_bar('gene_mutation_perc_chart_container', point_width, title_position);
    new Highcharts.Chart(stacked_bar_options);

    // Copy number percentages
    stacked_bar_options = hc_stacked_bar('gene_cn_perc_chart_container', point_width, title_position);
    new Highcharts.Chart(stacked_bar_options);
}