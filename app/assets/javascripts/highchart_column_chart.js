function hc_stacked_column(chart_id) {
    logger("hc_stacked_column(" + chart_id + ")");

    var x_categories = $('#'+chart_id).data('x-categories');
    var chart_series = $('#'+chart_id).data('chart-series');
    var title_text   = $('#'+chart_id).data('title-text');
    var chart_colors = $('#'+chart_id).data('chart-colors');

    stacked_column_options = {
        chart: {
            type: 'column',
            renderTo: chart_id
        },
        title: {
            text: title_text
        },
        xAxis: {
            categories: x_categories
        },
        yAxis: {
            min: 0,
            max: 100,
            title: {
                text: 'Phylum%'
            },
            stackLabels: {
                enabled: false
            }
        },
        legend: {
            align: 'right',
            x: -30,
            verticalAlign: 'top',
            y: 25,
            floating: true,
            backgroundColor:
            Highcharts.defaultOptions.legend.backgroundColor || 'white',
            borderColor: '#CCC',
            borderWidth: 1,
            shadow: false
        },
        tooltip: {
            headerFormat: '<b>{point.x}</b><br/>',
            pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
        },
        plotOptions: {
            column: {
                stacking: 'normal',
                dataLabels: {
                    enabled: false
                }
            }
        },
        credits: {
            enabled: false
        },
        series: chart_series
    }
    return(stacked_column_options)
}
