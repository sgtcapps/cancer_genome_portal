// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.

// For excel jquery button, require jszip.min.js (after dataTables.buttons, and before buttons.html5)

//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require bootstrap/dropdown
//= require bootstrap/carousel
//= require bootstrap/modal
//= require bootstrap/collapse
//= require bootstrap/tooltip
//= require bootstrap/tab
//= require bootstrap-typeahead-rails
//= require bootstrap-sprockets
//= require highcharts
//= require highcharts/modules/data.js
//= require highcharts/modules/exporting.js
//= require highcharts/modules/histogram-bellcurve.js
//= require highcharts/modules/heatmap.js
//= require highcharts/modules/sankey.js
//= require highcharts/highcharts-more
//= require dataTables/jquery.dataTables
//= require dataTables/bootstrap/3/jquery.dataTables.bootstrap
//= require dataTables.buttons.min.js
//= require buttons.flash.min.js
//= require buttons.html5.min.js
//= require buttons.print.min.js
//= require jquery.dataTables.sorting.natural.js
//= require_tree .
