function welcome_index_init() {
    logger("welcome_index_init()");

    $("#study_query_form").click(function() {
        $("#study_error").hide();
        $("#blank_param_error").hide();
        $("#invalid_param_error").hide();
    });

    $('#selectAll').click(function() {
        if (this.checked) {
            $(':checkbox').each(function() {
                this.checked = true;
            });
        } else {
            $(':checkbox').each(function() {
                this.checked = false;
            });
        }
    });

    $('.explore_query').click(function() {
        $("input[type=checkbox]").prop('checked', false);
        $('.btn-query').removeClass('active');
        $("#query_params_gene_name").val('');
        $("#query_params_neoantigen").val('');
        $("#query_params_patient_id").val('');
        $('#query_params_explore').val("T")
    });

    $('.btn-query').click(function() {
        $('.btn-query').removeClass('active').attr('selected',false);
        $(this).addClass('active').attr('selected',true);

        var study_ids = [];
        $("input:checkbox.checkbox-study:checked").each(function() {
            study_ids.push($(this).val());
        });

        //Need to trap error if patient query, but no studies selected
        $.ajax({
            url: "param_entry",
            type: "POST",
            data: { element_id: this.id, studies: study_ids },
            dataType: 'script'
        });
    });

    $("#submit_query").click(function() {
        if($('input:checkbox.checkbox-study:checked').length<1) {
            $("#blank_param_error").show().text('Please select one or more studies');
            return false
        }
        else if ($("#target_gene").hasClass("active")) {//
            if ($("#query_params_gene_name").val() == '') {
                $("#blank_param_error").show().text('Please enter one or more genes');
                return false
            }
        }
        else if ($("#target_neoantigen").hasClass("active")) {//
            if ($("#query_params_neoantigen").val() == '') {
                $("#blank_param_error").show().text('Please select a neoantigen');
                return false
            }
        }
        else if ($("#target_patient").hasClass("active")) {//
            if ($("#query_params_patient_id").val() == '') {
                $("#blank_param_error").show().text('Please select a patient number');
                return false
            }
        }
        else if($('input:checkbox.checkbox-study:checked').length>0 && !$("#target_gene").hasClass("active") && !$("#target_neoantigen").hasClass("active") && !$("#target_patient").hasClass("active")) {
            $("#blank_param_error").show().text('Please select a Gene, Neoantigen or Patient to query');
            return false
        }
        else {
        }
    });
}
