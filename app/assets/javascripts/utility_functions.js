function toggleDiv(button_div, div1_id, div2_id) {
    $('#' + button_div).find('button').click(function() {
        $(this).addClass('active').siblings().removeClass('active');
        var ix = $(this).index();
        $('#' + div1_id).toggle( ix === 0 );
        $('#' + div2_id).toggle( ix === 1 );
    });
} // function

function indelTooltip() {
    $('[data-toggle="tooltip"]').each(function () {
        $('.long-indel').tooltip({placement: "right", delay: 0});
    });
}

function geneFilter(radio_name, table, filter_cols) {
    //filter_cols is 4 element array [na, cancer-assoc, onocogene, TSG] representing datatable column numbers
    //In example above: 3=cancer-associated column, 5=oncogene column, 6=TSG column
    $(".gene_radio").change(function () {
        var val = $("option[name='"+radio_name+"']:selected").val();
        if (val == 0) {
            table.columns().search( '' ).draw();
        }
        else if (filter_cols[val] > 0) {
            table.columns().search( '' )
            table.columns(filter_cols[val]).search('Y').draw();
        }
    });
}
// From LIMS site:
// logger function
function logger(msg) {
    if (console) {
        console.log(msg);
    }
}

// common alert function if different from browser
function my_alert(msg) {
    if (typeof alertify === 'undefined') {
        alert(msg);
    } else {
        alertify.alert(msg);
    }
}
