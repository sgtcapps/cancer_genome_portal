class SampleRnaExpr < ApplicationRecord
  self.table_name = 'sample_rna_expr'
  belongs_to :cancer_study
  belongs_to :sample
  belongs_to :gene, foreign_key: :gene_name

  TABLE_HDGS = %w(Cancer Study Gene Patient SampleType FPKM Expr_Level)
  TABLE_FLDS = %w(cancer_abbr study gene_name patient_nr sample_type htseq_fpkm expr_level)

end
