class GeneSummary < ApplicationRecord
  belongs_to :cancer_study
  belongs_to :gene, foreign_key: :gene_name

  def tot_mutations
    num_missense + num_frameshift + num_inframe_indel + num_nonsense +
        num_splice + num_intron + num_utr_flank + num_silent + num_other_mut
  end

  def summary_other_mut
    tot_mutations - (num_missense + num_frameshift)
  end

end
