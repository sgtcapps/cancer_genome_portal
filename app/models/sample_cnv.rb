class SampleCnv < ApplicationRecord
  belongs_to :cancer_study
  belongs_to :sample
  belongs_to :gene, foreign_key: :gene_name

  TABLE_HDGS = %w(Cancer Study Gene Patient SampleType CN CN_log2 FPKM ExprLevel)
  TABLE_FLDS = %w(cancer_abbr study gene_name patient_nr sample_type copy_num log2_fc htseq_fpkm expr_level)

  CN_TRANSLATE = {-1 => 'CN deletion', 0 => 'No CNV', 1 => 'CN amplification'}

  def cnv_text
    (copy_num < 1 ? 'del' : 'amp')
  end

  def self.cnv_tab_query(where_condition)
    sql = <<SQL
    select cancer_studies.cancer_abbr, cancer_studies.study, sample_cnvs.*, rna.htseq_fpkm, rna.expr_level
    from sample_cnvs 
    join cancer_studies on cancer_studies.id = sample_cnvs.cancer_study_id 
    left join sample_rna_expr rna on (sample_cnvs.sample_id = rna.sample_id
                     and sample_cnvs.sample_type = rna.sample_type and sample_cnvs.gene_name = rna.gene_name)
    #{where_condition}
    group by sample_cnvs.id;
SQL
    result = ActiveRecord::Base.connection.exec_query(sql)
    col_indices = self::TABLE_FLDS.map{|fld| result.columns.find_index(fld)}
    [col_indices, result.rows]
  end
end