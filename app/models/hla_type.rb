class HlaType < ApplicationRecord
  has_many :patient_hla_types
  has_many :patients, through: :patient_hla_types
end
