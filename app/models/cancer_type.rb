class CancerType < ApplicationRecord
  has_many :cancer_studies

  #Create grouped downdowm using linked cancer_studies table, and collection mapping
  def self.cancer_study_dropdown
    return self.make_opt_pairs(self.grouped_studies)
    #output eg:  [["Bowel", [["Colon adenocarcinoma|IMH", 1], ["Colon adenocarcinoma|TCGA", 2]]],..
  end

  def self.grouped_studies
    self.select(:id, :cancer_group, :cancer_description, :cancer_abbr, 'cancer_studies.total_samples')
        .includes(:cancer_studies).references(:cancer_studies)
        .where("studies is not NULL")
        .order("cancer_group, cancer_description, cancer_studies.study").all.group_by(&:cancer_group)
  end

  #Create grouped dropdown using nested collect; opt value is cancer_studies.id
  def self.make_opt_pairs(grp_ctypes)
    #"Esophagus/Stomach", [#<CancerType id: 24, cancer_group: "Esophagus/Stomach", cancer_abbr: "ESCA", cancer_description: "Esophageal carcinoma",
    #                      #<CancerType id: 1, cancer_group: "Esophagus/Stomach", cancer_abbr: "STAD", cancer_description: "Stomach Adenocarcinoma"]
    return grp_ctypes.collect {|grp, ctypes| [grp, ctypes.collect{|ctype| ctype.cancer_studies.collect {
        |cstudy| [[ctype.cancer_description, cstudy.study].join('|'), cstudy.id, cstudy.total_samples]}}.flatten(1)]}
  end

end
