class Sample < ApplicationRecord
  belongs_to :cancer_study

  has_many :sample_mutations
  has_many :sample_cnvs
  has_many :sample_rna_exprs
  has_many :sample_immune_cells

end
