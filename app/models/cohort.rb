class Cohort < ApplicationRecord
  belongs_to :cancer_study
  has_many :samples
  has_many :cohort_samples

  def release_mth_yr
    release_date.strftime("%b %Y")
  end

  def self.latest_release
    self.order(release_num: :desc).limit(1).pluck(:release_num, :release_date)[0]
  end
end