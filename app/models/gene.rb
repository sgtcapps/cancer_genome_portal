class Gene < ApplicationRecord
  self.primary_key = 'gene_name'
  has_many :gene_summaries, foreign_key: :gene_name

  def self.all_genes
    self.all.pluck(:gene_name)
  end

  def cancer_gene
    cancer_assoc == 'Y' ? 'Y' : 'N'
  end
end