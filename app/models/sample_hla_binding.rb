class SampleHlaBinding < ApplicationRecord
  belongs_to :hla_type
  belongs_to :sample_neoantigen

  def self.hla_dropdown
     self.distinct.pluck(:hla_allele, :hla_type_id).sort
  end

  def binding_strength
    (binding_rank > 0.5 ? 'weak' : 'strong')
  end
end