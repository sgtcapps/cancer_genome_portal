class SampleMutation < ApplicationRecord
  belongs_to :cancer_study
  belongs_to :sample
  belongs_to :gene, foreign_key: :gene_name
  has_many :sample_neoantigens

  TABLE_HDGS = %w(Cancer Study Gene Patient SampleType Chr Pos RefAllele AltAllele
                  Classification VarType AAChange CN FPKM ExprLevel Neoantigens MutTPM
                  ChrPos Ref/Alt Neoantigen HLA-Allele BindingRank)
  TABLE_FLDS = %w(cancer_abbr study gene_name patient_nr sample_type chr chr_start ref_allele alt_allele
                  var_classification var_type protein_change copy_num htseq_fpkm expr_level nr_neoantigens mutation_tpm
                  chr_pos ref_alt neoantigen_9mer hla_allele binding_rank)

  def self.mut_tab_query(where_condition)
    sql = <<SQL
    select cancer_studies.cancer_abbr, cancer_studies.study, sample_mutations.*, cnv.copy_num, rna.htseq_fpkm, rna.expr_level, nr_neoantigens
    from sample_mutations 
    join cancer_studies on cancer_studies.id = sample_mutations.cancer_study_id
    left join sample_cnvs cnv on (sample_mutations.sample_id = cnv.sample_id  
                     and sample_mutations.sample_type = cnv.sample_type and sample_mutations.gene_name = cnv.gene_name)
    left join sample_rna_expr rna on (sample_mutations.sample_id = rna.sample_id
                     and sample_mutations.sample_type = rna.sample_type and sample_mutations.gene_name = rna.gene_name)
    #{where_condition}
    group by sample_mutations.id;
SQL
    result = ActiveRecord::Base.connection.exec_query(sql)
    query_flds = self::TABLE_FLDS[0..16]
    col_indices = query_flds.map{|fld| [self::TABLE_FLDS.find_index(fld), result.columns.find_index(fld)]}
    [col_indices, result.rows]
  end

  def self.neoantigen_tab_query(where_condition)
    sql = <<SQL
    select cancer_studies.cancer_abbr, cancer_studies.study, sample_mutations.*,
           neo.chr_pos, neo.ref_alt, neo.neoantigen_9mer, hla.hla_allele, hla.binding_rank
    from sample_mutations 
    join cancer_studies on cancer_studies.id = sample_mutations.cancer_study_id
    join sample_neoantigens neo on (sample_mutations.id = neo.sample_mutation_id)
    join sample_hla_bindings hla on (neo.id = hla.sample_neoantigen_id)
    #{where_condition}
    order by neo.chr_pos, neo.neoantigen_9mer;
SQL
    result = ActiveRecord::Base.connection.exec_query(sql)
    query_flds = %w(cancer_abbr study gene_name patient_nr sample_type chr_pos var_classification var_type
                    ref_alt protein_change mutation_tpm neoantigen_9mer hla_allele binding_rank)
    col_indices = query_flds.map{|fld| [self::TABLE_FLDS.find_index(fld), result.columns.find_index(fld)]}
    [col_indices, result.rows]
  end
end
