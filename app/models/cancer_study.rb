class CancerStudy < ApplicationRecord
  belongs_to :cancer_type
  has_many :cohorts
  has_many :gene_summaries

  STUDY_ID_NAMES = self.all.map{|cs| [cs.id, [cs.cancer_abbr, cs.study].join('|)')]}

  scope :has_details, -> { where("details_loaded = 'Y'") }

  def show_patient_age?
    study != 'GCR'
  end

  def study_name
    [cancer_abbr, study].join('|')
  end

  def self.study_names(study_ids)
    self.where("id in (?)", study_ids).map{|cstudy| cstudy.study_name}
  end

  def self.study_dropdown
    grp_cancers = self.select(:study, :cancer_type_id, :total_samples)
                  .includes(:cancer_type).references(:cancer_type)
                  .order("study, cancer_types.cancer_description").all.group_by(&:study)
    return grp_cancers.collect {|grp, studies| [grp, studies.collect{|study| [study.cancer_type.cancer_description, study.id, study.total_samples]}]}
    #output eg: [["IPG", [["Colon adenocarcinoma", 1, 177]]], ["TCGA", [["Colon adenocarcinoma", 3, 459], ["Esophageal carcinoma", 3, 184],...
  end
end
