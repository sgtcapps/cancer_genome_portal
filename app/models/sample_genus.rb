class SampleGenus < ApplicationRecord
  belongs_to :cancer_study
  belongs_to :sample

  def self.genus_cts(sample_ids)
    self.where({sample_id: sample_ids}).order('read_cts DESC').all
  end

  def self.sample_genus_rds(study_id, genus)
    self.where({cancer_study_id: study_id, taxo_parent: "g__#{genus}"})
        .group(:sample_id, :taxo_parent).sum(:read_cts)
  end
end
