class SampleTaxonomy < ApplicationRecord
  belongs_to :cancer_study
  belongs_to :sample

  def self.sankey_data(sample_ids)
    self.where({is_top10: 'Y', sample_id: sample_ids}).order('taxonomy_level, taxonomy_row')
        .pluck(:taxo_parent, :taxo_name, :read_cts)
  end

  def self.all_phylum(study_id)
    phylum_cts = self.where({cancer_study_id: study_id, taxonomy_level: 1})
                     .group(:taxo_name).sum(:read_cts).to_a
    phylum_cts.sort_by(&:last).map{|p| p[0]}
  end

  def self.sample_bacteria_rds(study_id)
    # Might want to change taxonomy_level filter to taxo_parent == 'k__Bacteria'? to exclude k__Archaea?
    self.where({cancer_study_id: study_id, taxonomy_level: 1})
        .group(:sample_id).sum(:read_cts)
  end

  def self.sample_phylum_rds(study_id, phylum)
    self.where({cancer_study_id: study_id, taxonomy_level: 1, taxo_name: "p__#{phylum}"})
        .pluck(:sample_id, :taxo_name, :read_cts)
  end
end
