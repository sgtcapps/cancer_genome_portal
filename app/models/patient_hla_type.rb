class PatientHlaType < ApplicationRecord
  belongs_to :patient
  belongs_to :hla_type
end
