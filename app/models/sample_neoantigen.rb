class SampleNeoantigen < ApplicationRecord
  belongs_to :sample_mutation
  has_many :sample_hla_bindings
end