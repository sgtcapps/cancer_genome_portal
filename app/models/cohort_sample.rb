class CohortSample < ApplicationRecord
  belongs_to :cohort

  def has_wgs?
    wgs_release != 'NA'
  end

  def has_wes?
    wes_release != 'NA'
  end

  def has_rna?
    rna_release != 'NA'
  end

end
