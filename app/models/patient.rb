class Patient < ApplicationRecord
  belongs_to :cancer_study
  has_many :samples
  has_many :patient_hla_types
  has_many :hla_types, through: :patient_hla_types

  def days_survival_or_na
    days_survival > 0 ? days_survival : 'NA'
  end

  def gc_history
    case family_history
    when 'Y'
      'Gastric Cancer'
    when 'N'
      'No Gastric Cancer'
    else
      'Unknown'
    end
  end

  def self.populate_dropdown_grouped(study_ids)
    patients_by_study = self.where('cancer_study_id IN (?)', study_ids)
                            .includes(:cancer_study).references(:cancer_study)
                            .select('patients.id, cancer_abbr, study, patient_nr').order('cancer_abbr, study, patient_nr')
                            .all.group_by{|p| [p.cancer_study.cancer_abbr, p.cancer_study.study].join('|')}
    return patients_by_study.collect {|cancer_study, attrs| [cancer_study, attrs.collect{|attr| [attr.patient_nr, attr.id]}]}
  end
end
