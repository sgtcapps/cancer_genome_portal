class SampleImmuneCell < ApplicationRecord
  belongs_to :cancer_study
  belongs_to :sample

  CIBERSORT_HDGS = {'b_naive' => 'B cells naive', 'b_memory' => 'B cells memory', 'plasma' => 'Plasma cells',
                    't_cd8' => 'T cells CD8', 't_cd4_naive' => 'T cells CD4 naive',
                    't_cd4_mem_resting' => 'T cells CD4 memory resting', 't_cd4_mem_activated' => 'T cells CD4 memory activated',
                    't_follicular_helper' => 'T cells follicular helper', 't_regulatory' => 'T regulatory',
                    't_gamma_delta' => 'T cells gamma delta', 'nk_resting' => 'NK cells resting',
                    'nk_activated' => 'NK cells activated', 'monocyte' => 'Monocytes', 'macrophage_m0' => 'Macrophages M0',
                    'macrophage_m1' => 'Macrophages M1', 'macrophage_m2' => 'Macrophages M2',
                    'dendritic_resting' => 'Dendritic cells resting', 'dendritic_activated' => 'Dendritic cells activated',
                    'mast_resting' => 'Mast cells resting', 'mast_activated' => 'Mast cells activated',
                    'eosinophil' => 'Eosinophils', 'neutrophil' => 'Neutrophils'}

  def b_cells
    b_naive + b_memory + plasma
  end

  def t_cd4
    t_cd4_naive + t_cd4_mem_resting + t_cd4_mem_activated + t_follicular_helper + t_regulatory + t_gamma_delta
  end

  def nk_cells
    nk_resting + nk_activated
  end

  def macro_mono
    monocyte + macrophage_m0 + macrophage_m1 + macrophage_m2
  end

  def dendritic
    dendritic_resting + dendritic_activated
  end

  def mast_cells
    mast_resting + mast_activated
  end

end
