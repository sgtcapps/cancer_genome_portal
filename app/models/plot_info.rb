class PlotInfo
  PLOT_COLORS = {standard: ["#66C2A5" ,"#FC8D62", "#8DA0CB", "#E78AC3", "#A6D854", "#FFD92F", "#E5C494", "#C091BE"],
                 cibersort_22: ["#996600","#CC9933","#FFCC66","#FFCC00","#FFFF00","#CCFF00","#99FF00","#33FF00","#33CC00","#00FF33",
                                "#33FF66","#33CC66","#0099CC","#33CCFF","#66CCFF","#6699FF","#3366FF","#0033CC","#CC33FF","#9900CC",
                                "#663366","#993399"].reverse,
                 multi_20: ["#58b5e1", "#beb1e7", "#40cf9b", "#4c726a", "#97b47d", "#097b35", "#88cc1f", "#926026", "#f2a68c",
                            "#d04650", "#f79302", "#ff0087", "#fe5900", "#2f65d0", "#f59ae7", "#a3469d", "#f739e2", "#7c627b",
                            "#5951f3", "#00d618"],
                 multi_10: ["#a6cee3", "#1f78b4", "#b2df8a", "#33a02c", "#fb9a99", "#e31a1c", "#fdbf6f", "#ff7f00", "#cab2d6", "#6a3d9a"],
                 std_pie: ["#2987e3","#db3811","#f78508","#0f9617","#990199","#0099c7","#de4578","#66ab01","#b82e2e"],
                 ynu_pie: ["#2987e3","#db3811","#D2D2D2"],
                 box_plot: ["#66C2A5" ,"#FC8D62", "#8DA0CB", "#E78AC3", "#A6D854", "#FFD92F", "#E5C494", "#B3B3B3"],
                 histogram: ["#2986e2"],
                 gender_pink_blue: ["#e0699e","#2986e2"],
                 cn_pink_blue: ["#FFAEAE" ,"#B4D8E7"],
                 na_grey: ["#B3B3B3", "#D3D3D3", "#D2D2D2", "#B5B5B5"]
  }

  def self.gender_colors
    PLOT_COLORS[:gender_pink_blue] + [PLOT_COLORS[:na_grey][2]]
  end

  def self.cnv_colors
    PLOT_COLORS[:cn_pink_blue]
  end

  # Additional methods defined in plot_colors_helper.rb
  # Need to put in helper for any schemes where color array is variable (eg NA-grey in different positions)
end
