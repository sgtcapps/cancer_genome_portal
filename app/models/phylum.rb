class Phylum < ApplicationRecord
  self.table_name = 'phyla'
  COLOR_INDICES = self.all.pluck(:phylum, :color_index).to_h
end
