module GeneSummaryHelper
  def hla_bind_strength(binding_rank)
    (binding_rank > 0.5 ? 'weak' : 'strong')
  end
end
