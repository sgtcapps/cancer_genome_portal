module ExploreStudyHelper
  def onco_tsg(onco_flag, tsg_flag)
    if onco_flag.nil? and tsg_flag.nil?
      val = nil
    elsif onco_flag == 'Y' and tsg_flag == 'Y'
      val = 'Both'
    elsif onco_flag == 'Y'
      val = 'Onco'
    elsif tsg_flag == 'Y'
      val = 'TSG'
    else
      val = "&#10035;".html_safe
    end
  end
end
