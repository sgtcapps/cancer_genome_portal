module PlotColorsHelper
  #Note: be careful not to do direct assignment to PlotInfo::PLOT_COLORS[fld] if doing insertions
  #  New variable is assigned to actual PlotInfo class variable, and values inserted there

  def mutation_bar_colors(n)
    return (PlotInfo::PLOT_COLORS[:standard][0..n-2] + [PlotInfo::PLOT_COLORS[:na_grey][0]])
  end

  def expr_boxplot_colors
    self.mutation_bar_colors(8)
  end

  def pie_colors(idx_grey=nil, idx_grey2=nil)
    chart_colors = PlotInfo::PLOT_COLORS[:std_pie]*2
    idx_vals = [idx_grey, idx_grey2].reject { |idx| idx.nil? }
    unless idx_vals.empty?
      idx_vals.sort.each do |idx|
        c_idx = idx_vals.index(idx) #Use color based on order of values in param list (not sorted order)
        chart_colors.insert(idx, PlotInfo::PLOT_COLORS[:na_grey][c_idx+2])
      end
    end
    return chart_colors
  end

  def pie_ynu_colors(order)
    return order.map{|x| PlotInfo::PLOT_COLORS[:ynu_pie][x]}
  end

  def standard_colors(idx_grey=nil)
    chart_colors = PlotInfo::PLOT_COLORS[:standard] + PlotInfo::PLOT_COLORS[:box_plot]
    unless idx_grey.nil?
      chart_colors.insert(idx_grey, PlotInfo::PLOT_COLORS[:na_grey][1])
    end
    return chart_colors
  end
end
