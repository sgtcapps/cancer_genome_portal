module ApplicationHelper
  def full_title(page_title)
    base_title = Portal::Config::Info['base_title']
    return (page_title.empty? ? base_title : "#{page_title} | #{base_title}")
  end
  
  def flash_class(level)
    case level
        when :notice then "alert alert-danger"
        when :success then "alert alert-success"
        when :error then "alert alert-error"
        when :alert then "alert alert-error"
    end
  end

  def humanize_boolean(tf_value)
    tf_value ? 'Yes' : 'No'
  end

  def truncate_with_ellipsis(fld, max_len=4)
    fld.strip.truncate(max_len, omission: '...')
  end

  def format_pct(numerator, denominator, ndecimals=1)
    if (numerator.nil? or numerator == 0)
      return 0
    elsif (denominator.nil? or denominator == 0)
      return 'Inf'
    else
      return ((numerator / denominator.to_f)*100).round(ndecimals)
    end
  end

=begin  
  def breaking_word_wrap(text, *args)
    options = args.extract_options!
    unless args.blank?
     options[:line_width] = args[0] || 80
    end
    options.reverse_merge!(:line_width => 80)
    text = text.split(",").collect do |word|
     word.length > options[:line_width] ? word.gsub(/(.{1,#{options[:line_width]}})/, "\\1 ") : word
    end * " "
    text.split("\n").collect do |line|
     line.length > options[:line_width] ? line.gsub(/(.{1,#{options[:line_width]}})(\s+|$)/, "\\1\n").strip : line
    end * "\n"
  end
=end
  
end
