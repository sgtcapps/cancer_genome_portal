# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 0) do

  create_table "auth_users", id: :integer, limit: 2, force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=latin1" do |t|
    t.string "org_name", limit: 15, null: false
    t.string "name", limit: 50, null: false
    t.string "auth_email", limit: 75, null: false
    t.string "notes"
    t.datetime "created_at", null: false
    t.timestamp "updated_at"
  end

  create_table "cancer_studies", id: :integer, unsigned: true, force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=latin1" do |t|
    t.integer "cancer_type_id", limit: 2, unsigned: true
    t.string "cancer_abbr", limit: 4
    t.string "study", limit: 4
    t.string "details_loaded", limit: 1
    t.integer "total_samples", unsigned: true
    t.integer "num_mut_samples", unsigned: true
    t.integer "num_cn_samples", unsigned: true
    t.integer "num_rna_samples", unsigned: true
  end

  create_table "cancer_types", id: :integer, unsigned: true, force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.string "cancer_group", limit: 25
    t.string "cancer_abbr", limit: 12
    t.string "cancer_description", limit: 50
    t.string "studies", limit: 50
    t.timestamp "created_at"
    t.timestamp "updated_at"
    t.index ["cancer_abbr"], name: "index_cancer_types_on_abbreviation", unique: true
    t.index ["cancer_description"], name: "index_cancer_types_on_name", unique: true
  end

  create_table "gene_summaries", id: :integer, force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=latin1" do |t|
    t.integer "cancer_study_id", limit: 2, unsigned: true
    t.string "study", limit: 4
    t.string "cancer_abbr", limit: 8
    t.string "gene", limit: 50
    t.integer "tot_rna_sample", limit: 2, unsigned: true
    t.integer "num_high_exp_sample", limit: 2, unsigned: true
    t.integer "num_medium_exp_sample", limit: 2, unsigned: true
    t.integer "num_low_exp_sample", limit: 2, unsigned: true
    t.integer "tot_mut_sample", limit: 2, unsigned: true
    t.integer "num_missense", limit: 2, unsigned: true
    t.integer "num_frameshift", limit: 2, unsigned: true
    t.integer "num_other_mut", limit: 2, unsigned: true
    t.integer "tot_cnv_sample", limit: 2, unsigned: true
    t.integer "num_amp_sample", limit: 2, unsigned: true
    t.integer "num_del_sample", limit: 2, unsigned: true
    t.datetime "created_at"
    t.timestamp "modified_at"
    t.index ["cancer_study_id"], name: "fk_gs_cancer_study"
    t.index ["gene"], name: "idx_gs_gene"
  end

  create_table "genes", id: :integer, unsigned: true, force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=latin1" do |t|
    t.string "gene", limit: 50
    t.integer "hugo", limit: 1
    t.string "clin_relevance", limit: 20
  end

  create_table "sample_cnvs", id: :integer, unsigned: true, force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=latin1" do |t|
    t.integer "cancer_study_id", limit: 2, unsigned: true
    t.string "cancer_abbr", limit: 4
    t.string "study", limit: 8
    t.string "gene", limit: 50
    t.string "sample", limit: 20
    t.string "sample_type", limit: 2
    t.integer "copy_num", limit: 1
    t.index ["cancer_study_id"], name: "cn_idx_study_id"
    t.index ["gene"], name: "cn_idx_gene"
    t.index ["sample"], name: "cn_idx_sample"
  end

  create_table "sample_mutations", id: :integer, unsigned: true, force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=latin1" do |t|
    t.integer "cancer_study_id", limit: 2, unsigned: true
    t.string "cancer_abbr", limit: 4
    t.string "study", limit: 8
    t.string "gene", limit: 50
    t.string "sample", limit: 20
    t.string "sample_type", limit: 2
    t.string "chr", limit: 5
    t.integer "chr_start"
    t.integer "chr_end"
    t.string "ref_allele"
    t.string "alt_allele"
    t.string "var_classification", limit: 25
    t.string "var_type", limit: 3
    t.string "protein_change"
    t.index ["cancer_study_id"], name: "mt_idx_study_id"
    t.index ["gene"], name: "mt_idx_gene"
    t.index ["sample"], name: "mt_idx_sample"
  end

  create_table "sample_rna_expr", id: :integer, unsigned: true, force: :cascade, options: "ENGINE=MyISAM DEFAULT CHARSET=latin1" do |t|
    t.integer "cancer_study_id", limit: 2, unsigned: true
    t.string "cancer_abbr", limit: 4
    t.string "study", limit: 8
    t.string "gene", limit: 50
    t.string "sample", limit: 20
    t.string "sample_type", limit: 2
    t.decimal "htseq_fpkm", precision: 8, scale: 2
    t.string "expr_level", limit: 6
    t.index ["cancer_study_id"], name: "rna_idx_study_id"
    t.index ["gene"], name: "rna_idx_gene"
    t.index ["sample"], name: "rna_idx_sample"
  end

  create_table "users", force: :cascade, options: "ENGINE=InnoDB DEFAULT CHARSET=latin1" do |t|
    t.integer "auth_user_id", null: false, unsigned: true
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

end
