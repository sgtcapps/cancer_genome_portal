# Cancer Genome Portal

This is a Ruby on Rails application for providing an integrative genomic analysis for clinically relevant cancer genetic aberration and targeted therapeutic prediction.

This application is dependent on a MySQL database and currently there is no populated seeds.rb.

NOTE: This is a work in progress.